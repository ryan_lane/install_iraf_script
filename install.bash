# YOU MUST SOURCE THIS SCRIPT IN BASH
# EG:  source ./install.bash
# THANKS

# VARIABLES
export RSYNC_PASSWORD="pub"
export IRAF="~/iraf/"

# this clears the terminal window
clear

# make software installation directory
mkdir -p ~/iraf

# change to that directory
cd ~/iraf

# rsync to copy iraf install files to local machine
echo "ladeedeedada..."
rsync rsync://pub@vpc-admin/pub/iraf-macosx.tar.gz /tmp
echo done
echo

# unpack (the tar gzip file) to the working directory
echo "letsunpackit..."
tar xzf /tmp/iraf-macosx.tar.gz
echo done

# run the installer script now
echo 'export IRAF="~/iraf/"' >> ~/.bashrc
echo "export PATH=~/iraf/vendor/x11iraf/bin.macintel:$PATH" >> ~/.bashrc
./install

# celebrate victory
echo
echo
echo "TADA ALL Done, Congrats!"
